from datetime import timedelta
from odoo import api, fields, models

BOOK_STATE = [
    ('draft', 'Not Available'),
    ('available', 'Available'),
    ('lost', 'Lost')
]


class LibraryBook(models.Model):
    _name = 'library.book'
    _description = 'Library Book'
    _order = 'date_release desc, name'
    _rec_name = 'short_name'
    _sql_constraints = [
        ('name_uniq', 'UNIQUE (name)', 'Book title must be unique!'),
        ('positive_page', 'CHECK(pages>0)', 'No of pages must be positive!')
    ]

    name = fields.Char('Title', required=True)
    short_name = fields.Char(
        string='Short Title',
        translate=True,
        index=True,
    )

    notes = fields.Text('Internal Notes')
    state = fields.Selection(
        selection=BOOK_STATE,
        string='State',
        default='draft',
    )
    description = fields.Html(
        string='Description',
        sanitize=True,
        strip_style=False,
    )
    cover = fields.Binary('Book Cover')
    out_of_print = fields.Boolean('Out of Print?')
    date_release = fields.Date('Release Date')
    date_updated = fields.Datetime('Last Updated')
    pages = fields.Integer(
        string='Number of Pages',
        groups='base.group_user',
        states={'lost': [('readonly', True)]},
        help='Total book page count',
        company_dependent=False,
    )
    reader_rating = fields.Float(
        string='Reader Average Rating',
        digits=(14, 4),  # Optional precision decimals,
    )
    author_ids = fields.Many2many(
        comodel_name='res.partner',
        string='Authors',
    )
    cost_price = fields.Float(
        string='Book Cost',
        digits='Book Price',
    )
    currency_id = fields.Many2one(
        comodel_name='res.currency',
        string='Currency',
    )
    retail_price = fields.Monetary(
        string='Retail Price',
        currency_field='currency_id',
    )
    publisher_id = fields.Many2one(
        comodel_name='res.partner',
        string='Publisher',
        # optional:
        ondelete='set null',
        context={},
        domain=[],
    )
    publisher_city = fields.Char(
        string='Publisher City',
        related='publisher_id.city',
        readonly=True,
    )

    category_id = fields.Many2one(
        comodel_name='library.book.category',
        string='Category'
    )
    age_days = fields.Float(
        string='Days Since Release',
        compute='_compute_age',
        inverse='_inverse_age',
        search='_search_age',
        store=False,
        compute_sudo=True  # optional
    )

    ref_doc_id = fields.Reference(
        selection='_referencable_models',
        string='Reference Document',
    )
    @api.model
    def _referencable_models(self):
        my_models = self.env['ir.model'].search([
            ('field_id.name', '=', 'message_ids')])
        return [(x.model, x.name) for x in my_models]


    def name_get(self):
        result = []
        for record in self:
            rec_name = "%s (%s)" % (record.name, record.date_release)
            result.append((record.id, rec_name))
        return result

    @api.constrains('date_release')
    def _check_release_date(self):
        for record in self:
            if record.date_release and record.date_release > fields.Date.today():
                raise models.ValidationError('Release date must be in the past')

    @api.depends('date_release')
    def _compute_age(self):
        today = fields.Date.today()
        for book in self:
            if book.date_release:
                delta = today - book.date_release
                book.age_days = delta.days
            else:
                book.age_days = 0

    def _inverse_age(self):
        today = fields.Date.today()

        for book in self.filtered('date_release'):
            d = today - timedelta(days=book.age_days)
            book.date_release = d

    def _search_age(self, operator, value):
        today = fields.Date.today()

        value_days = timedelta(days=value)
        value_date = today - value_days
        # convert the operator:
        # book with age > value have a date < value_date
        operator_map = {
            '>': '<', '>=': '<=',
            '<': '>', '<=': '>=',
        }
        new_op = operator_map.get(operator, operator)
        return [('date_release', new_op, value_date)]


class ResPartner(models.Model):
    _inherit = 'res.partner'
    published_book_ids = fields.One2many(
        comodel_name='library.book',
        inverse_name='publisher_id',
        string='Published Books',
    )
    authored_book_ids = fields.Many2many(
        comodel_name='library.book',
        string='Authored Books',
        # relation='library_book_res_partner_rel'
    )
