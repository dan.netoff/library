from odoo import api, fields, models


class BookCategory(models.Model):
    _name = 'library.book.category'
    _parent_store = True

    name = fields.Char(string='Category')
    parent_id = fields.Many2one(
        comodel_name='library.book.category',
        string='Parent Category',
        ondelete='restrict',
        index=True,
    )
    child_ids = fields.One2many(
        comodel_name='library.book.category',
        inverse_name='parent_id',
        string='Child Categories',
    )
    parent_path = fields.Char(index=True)

    # NOT WORK
    # @api.constrains('parent_id')
    # def _check_hierarchy(self):
    #     print('HOOOLA FROM _check_hierarchy')
    #     if not self._check_recursion():
    #         print('HOOOLA not self._check_recursion():')
    #         raise models.ValidationError(
    #             'Error! You cannot create recursive categories.')
