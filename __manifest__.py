{
    'name': "Library",
    'license': 'LGPL-3',
    'author': "Dan22",
    'website': "",
    'category': 'Productivity',
    'version': '15.0.1.0.1',
    'depends': ['base'],
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/library_book.xml',
        'views/library_book_categ.xml',
    ],
    'installable': True,
    'application': True,
}
